# Alfred RubyGems Search

An [Alfred](https://www.alfredapp.com/) workflow which lets you search RubyGems.org with ease!

Also available on [Packal](https://www.packal.org/)

Source Code hosted on [Gitlab](https://gitlab.com/mateuszdrewniak/alfred-rubygems-search)

## Features

- Live search of gems from RubyGems.org
- Open gem repositories, documentations, and RubyGems pages in your browser (or with QuickLook)
- Fetch all available data regarding a particular gem

![Demo](assets/readme/demo.gif)

## Commands

- `gems <gem name>` -- search for all gems with a similar name. Press return/enter on any gem on the list to open it's **RubyGems page** in your browser.
- `gemsd <gem name>` -- search for all gems with a similar name. Press return/enter on any gem on the list to open it's **Documentation** in your browser.
- `gemsr <gem name>` -- search for all gems with a similar name. Press return/enter on any gem on the list to open it's **Repository** in your browser.
- `gem <gem name>` -- search for a particular gem with this exact name. A list of various attributes of the gem will appear. Press return/enter on any attribute to either open it in your browser (if its value is a link) or copy its value to the clipboard
