#! /usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require_relative 'services/ruby_gems'

::RubyGems.print_gem_details(::ARGV[0])
