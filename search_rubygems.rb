#! /usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require_relative 'services/ruby_gems'

::RubyGems.print_search_results(::ARGV[0]) do |gem|
  gem['project_uri']
end
