# frozen_string_literal: true

module Helpers
  # @param number [Numeric]
  # @return [String]
  def self.number_to_human(number)
    number_string = number.to_s
    len = number_string.length

    if len > 12
    "#{number_string[0..-13]}Tn"
    elsif len > 9
      "#{number_string[0..-10]}Bn"
    elsif len > 6
      "#{number_string[0..-7]}M"
    elsif len > 3
      "#{number_string[0..-4]}K"
    else
      number_string
    end
  end

  def self.number_to_space_separated(number)
    number_string = number.to_s
    return number_string unless number_string.length > 3

    (number_string.length - 3).step(1, -3) do |ind|
      number_string.insert(ind, ' ')
    end

    number_string
  end
end
