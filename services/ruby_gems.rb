# frozen_string_literal: true

require 'json'
require 'net/http'
require 'uri'

require_relative 'helpers'

module RubyGems
  # @return [URI::HTTP, URI::HTTPS]
  SEARCH_URI = URI('https://rubygems.org/api/v1/search.json').freeze
  # Proc that takes a single argument -- name of the gem in question
  # @return [Proc]
  GEM_DETAILS_URI = proc { |name| URI("https://rubygems.org/api/v1/gems/#{name}.json") }

  def self.uri_with_params(uri, params)
    uri = uri.dup
    uri.query = ::URI.encode_www_form(params)

    uri
  end

  # @param query [String]
  # @return [Array<Hash{String => Object}>]
  def self.search_for_gems(query)
    search_uri = uri_with_params(SEARCH_URI, { query: query })
    ::JSON.parse(::Net::HTTP.get_response(search_uri).body).first(15)
  end

  # @param query [String]
  # @return [void]
  def self.print_search_results(query)
    results = search_for_gems(query).map do |gem|
      uri = yield(gem)
      {
        uid: gem['name'],
        title: gem['name'],
        subtitle: "⬇#{::Helpers.number_to_human(gem['downloads'])} - 🔨#{gem['version']} - 📖 #{gem['info']}",
        quicklookurl: uri,
        description: gem['info'],
        autocomplete: gem['name'],
        arg: uri,
        action: {
          url: uri,
        },
      }
    end

    puts ::JSON.generate({ items: results })
  end

  # @param query [String]
  # @return [Hash{String => Object}]
  def self.get_gem_details(name)
    details_uri = GEM_DETAILS_URI.call(name)
    ::JSON.parse(::Net::HTTP.get_response(details_uri).body)
  rescue ::JSON::ParserError
    nil
  end

  def self.gem_attribute_to_hash(title, subtitle, uri: nil, action_text: nil, valid: true, icon: nil)
    result = {
      title: title,
      subtitle: subtitle,
      quicklookurl: uri,
      description: title,
      autocomplete: title,
      arg: uri,
      action: {},
      valid: valid
    }
    result[:arg] = action_text if action_text
    result[:action][:text] = action_text if action_text
    result[:action][:url] = uri if uri
    result[:icon] = { path: "assets/#{icon}" } if icon

    result
  end

  # @param [#to_s]
  # @return [Boolean]
  def self.present?(val)
    return false unless val

    !val.to_s.length.zero?
  end

  # @params name [String]
  # @return [void]
  def self.print_gem_details(name)
    gem = get_gem_details(name)
    return unless gem

    results = []
    downloads = ::Helpers.number_to_space_separated(gem['downloads'])
    results << gem_attribute_to_hash('Info', gem['info'], action_text: gem['info'], icon: 'open_book.png')
    results << gem_attribute_to_hash('RubyGems', gem['project_uri'], uri: gem['project_uri'])
    results << gem_attribute_to_hash('Repository', gem['source_code_uri'], uri: gem['source_code_uri'], icon: 'git.png') if present?(gem['source_code_uri'])
    results << gem_attribute_to_hash('Documentation', gem['documentation_uri'], uri: gem['documentation_uri'], icon: 'file_cabinet.png') if present?(gem['documentation_uri'])
    results << gem_attribute_to_hash('Homepage', gem['homepage_uri'], uri: gem['homepage_uri'], icon: 'home.png') if present?(gem['homepage_uri'])
    results << gem_attribute_to_hash('Wiki', gem['wiki_uri'], uri: gem['wiki_uri'], icon: 'magn_glass.png') if present?(gem['wiki_uri'])
    results << gem_attribute_to_hash('Bug Tracking', gem['bug_tracker_uri'], uri: gem['bug_tracker_uri'], icon: 'bug.png') if present?(gem['bug_tracker_uri'])
    results << gem_attribute_to_hash('Downloads', downloads, action_text: downloads, icon: 'download.png')
    results << gem_attribute_to_hash('Version', gem['version'], action_text: gem['version'], icon: 'version.png')
    results << gem_attribute_to_hash('Authors', gem['authors'], action_text: gem['authors'], icon: 'programmer.png')

    puts ::JSON.generate({ items: results })
  end

end
